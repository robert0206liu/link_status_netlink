#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <netinet/ip.h>
#include <net/if.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <linux/sockios.h>
#include <linux/netlink.h>  
#include <linux/rtnetlink.h>
#include <errno.h>

#define UEVENT_BUFFER_SIZE 2048

int main() {

    // LOCAL VARIABLES DECLARATION
    int ret = 0;
    struct sockaddr_nl client;  
    struct timeval tv;  
    int fd, rcvlen; 
    fd_set fds;
    char p_recv_buff[UEVENT_BUFFER_SIZE*2];
    int i;
    
    struct iovec iov = { p_recv_buff, sizeof(p_recv_buff) };
    struct sockaddr_nl snl;
    struct msghdr msg = { (void*)&snl, sizeof(snl), &iov, 1, NULL, 0, 0};
    struct nlmsghdr *p_nh;
    
    struct ifinfomsg *if_info;
    char ifname[IFNAMSIZ];
    
    // BODY
    
    memset(p_recv_buff, 0, sizeof(p_recv_buff));
    
    // Create socket for grabbing NETLINK_ROUTE
    fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    if (fd == -1) {
        printf("Cannot create AF_NETLINK socket: %s.",  strerror(errno));
        goto error_exit_none_close;
    }
    
    memset(&client, 0, sizeof(client));  
    client.nl_family = AF_NETLINK;  
    client.nl_pid = getpid();  
    client.nl_groups = RTMGRP_LINK; // receive broadcast message
    
    if (bind(fd, (struct sockaddr*)&client, sizeof(client)) < 0) {
        printf("bind FAILED: %s.",  strerror(errno));
        goto error_exit;
    }

    printf("Start watching:\n");
    
    // Start waiting
    while ( 1 ) {
watching:
        FD_ZERO(&fds);  
        FD_SET(fd, &fds);  
        tv.tv_sec = 0;  
        tv.tv_usec = 100 * 1000;  
        ret = select(fd + 1, &fds, NULL, NULL, &tv);
        if(ret < 0) {
            continue;
        }
        if(!(ret > 0 && FD_ISSET(fd, &fds))) {
            continue;
        }
        
        /* receive data */
        rcvlen = recvmsg(fd, &msg, 0);  

        if(rcvlen < 0) {
            /* Socket non-blocking so bail out once we have read everything */
            if (errno != EWOULDBLOCK || errno != EAGAIN) {
                /* Anything else is an error */
                printf("read_netlink: Error recvmsg: %d\n", rcvlen);
                goto error_exit;
            }
        }
        
        if(rcvlen > 0) {
            for(p_nh = (struct nlmsghdr *) p_recv_buff; 
                NLMSG_OK(p_nh, (unsigned int)rcvlen); 
                p_nh = NLMSG_NEXT (p_nh, rcvlen)) {
                /* Finish reading */
                if (p_nh->nlmsg_type == NLMSG_DONE) {
                    goto watching;
                }

                /* Message is some kind of error */
                if (p_nh->nlmsg_type == NLMSG_ERROR)
                {
                    printf("read_netlink: Message is an error - decode TBD\n");
                    goto error_exit; // Error
                }

                /* Call message handler */
                if (p_nh->nlmsg_type == RTM_NEWLINK) {
                    if_info = NLMSG_DATA(p_nh);
                    if_indextoname(if_info->ifi_index,ifname);
                    if (strcmp(ifname, "eth1") == 0) {
                        printf("%s: Link %s\n", ifname, (if_info->ifi_flags & IFF_RUNNING) ? "Up" : "Down");
                    }
                }
            }
        }
        memset(p_recv_buff, 0, sizeof(p_recv_buff));
    }
    
error_exit:
    close(fd);
    
error_exit_none_close:
    return 0;
}